call plug#begin('~/.local/share/nvim/plugged')

"Plug 'eemed/sitruuna.vim'
"Plug 'sainnhe/edge'
Plug 'ghifarit53/tokyonight-vim'
Plug 'alvan/vim-closetag'
Plug 'preservim/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'pangloss/vim-javascript'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'peterhoeg/vim-qml'
Plug 'bfrg/vim-cpp-modern'

call plug#end()

" use colorscheme
set termguicolors
set hlsearch
filetype plugin indent on
syntax on

let g:tokyonight_style = 'night' " available: night, storm
let g:tokyonight_enable_italic = 1
colorscheme tokyonight

"show current commands
set showcmd

"always show status line
set laststatus=2

"show full current file path in the status line
set statusline+=%F\ %m


au BufRead,BufNewFile *.html set bomb

set mouse=a
"fix right side mouse issue
set ttymouse=sgr

set number

autocmd FileType cpp setlocal shiftwidth=4 softtabstop=4 expandtab
set tabstop=2
set shiftwidth=2
set expandtab

set encoding=utf-8

"let g:NERDTreeDirArrowExpandable = '?'
"let g:NERDTreeDirArrowCollapsible = '?'

set foldcolumn=1

"autocmd VimEnter * NERDTree
"autocmd VimEnter * wincmd p

set autochdir
let NERDTreeChDirMode=2
function! NTTFunction()
  NERDTreeToggle
  if (g:NERDTree.IsOpen()) 
    setlocal wincolor=NerdTreeCWD
  endif
endfunction
nnoremap <C-t> :call NTTFunction()<CR>

let &fillchars ..= ',eob: '

let NERDTreeShowHidden=1

"NERDTree git integration
let g:NERDTreeGitStatusIndicatorMapCustom = {
                \ 'Modified'  :'✹',
                \ 'Staged'    :'✚',
                \ 'Untracked' :'✭',
                \ 'Renamed'   :'➜',
                \ 'Unmerged'  :'═',
                \ 'Deleted'   :'✖',
                \ 'Dirty'     :'✗',
                \ 'Ignored'   :'☒',
                \ 'Clean'     :'✔︎',
                \ 'Unknown'   :'?',
                \ }
let g:NERDTreeGitStatusShowIgnored = 1
let g:NERDTreeGitStatusShowClean = 1

" --- AUTO CLOSE (X)HTML ---
"
" filenames like *.xml, *.html, *.xhtml, ...
" These are the file extensions where this plugin is enabled.
"
let g:closetag_filenames = '*.html,*.xhtml,*.phtml,*.njk'

" filenames like *.xml, *.xhtml, ...
" This will make the list of non-closing tags self-closing in the specified files.
"
let g:closetag_xhtml_filenames = '*.xhtml,*.jsx,*.njk'

" filetypes like xml, html, xhtml, ...
" These are the file types where this plugin is enabled.
"
let g:closetag_filetypes = 'html,xhtml,phtml,njk'

" filetypes like xml, xhtml, ...
" This will make the list of non-closing tags self-closing in the specified files.
"
let g:closetag_xhtml_filetypes = 'xhtml,jsx'

" integer value [0|1]
" This will make the list of non-closing tags case-sensitive (e.g. `<Link>` will be closed while `<link>` won't.)
"
let g:closetag_emptyTags_caseSensitive = 1

" Shortcut for closing tags, default is '>'
"
let g:closetag_shortcut = '>'

" Add > at current position without closing the current tag, default is ''
"xclip -o -selection clipboard
let g:closetag_close_shortcut = '<leader>>'

au BufReadPost *.njk set syntax=html


" Put plugins and dictionaries in this dir (also on Windows)
let vimDir = '$HOME/.vim/vim'
let &runtimepath.=','.vimDir

" Keep undo history across sessions by storing it in a file
if has('persistent_undo')
    let myUndoDir = expand(vimDir . '/undodir')
    " Create dirs
    call system('mkdir ' . vimDir)
    call system('mkdir ' . myUndoDir)
    let &undodir = myUndoDir
    set undofile
endif


set clipboard=unnamedplus

function! ClipboardYank()
  call system('wl-copy || xclip -i -selection clipboard', @@)
endfunction

let vlcb = 0
let vlce = 0
function! ClipboardPaste(mode)
  if (a:mode == "v")
    call cursor(g:vlcb[0], g:vlcb[1]) | execute "normal! v" | call cursor(g:vlce[0], g:vlce[1])  
  endif
  let @@ = system('wl-paste >/dev/null 2>/dev/null && wl-paste -n || xclip -o -selection clipboard')
endfunction

" replace currently selected text with default register without yanking it
vnoremap <silent>p "_dP

vnoremap <silent>y y:call ClipboardYank()<CR>
vnoremap <silent>d d:call ClipboardYank()<CR>
nnoremap <silent>dd dd:call ClipboardYank()<CR>

nnoremap <silent>p :call ClipboardPaste("n")<CR>p
vnoremap p :<C-U>let vlcb = getpos("'<")[1:2] \| let vlce = getpos("'>")[1:2] \| call ClipboardPaste("v")<CR>p


let NERDTreeMouseMode=2


nmap <C-S> :call <SID>SynStack()<CR>
function! <SID>SynStack()
    if !exists("*synstack")
        return
    endif
    echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunc

au InsertEnter * silent execute "!echo -en \<esc>[5 q"
au InsertLeave * silent execute "!echo -en \<esc>[2 q"

set colorcolumn=100
